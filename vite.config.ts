import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
  ],
  server: {
    host: '0.0.0.0',//ip地址
    port: 5200, // 设置服务启动端口号
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  }
})
