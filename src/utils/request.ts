import axios, { AxiosRequestConfig, AxiosResponse, ResponseType } from 'axios';
import pkg from 'query-string'
const {stringify} = pkg

const { BASE_URL } = import.meta.env;

const pendingAjax: any = new Map();

const axiosConfig: AxiosRequestConfig = {
  baseURL: BASE_URL,
  transformResponse: [
    function (data: AxiosResponse) {
      return data;
    },
  ],
  // 请求响应超时时间
  timeout: 1800000,
  responseType: 'json',
  headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  paramsSerializer: function (params) {
    return stringify(params);
  },
};

const AxiosInstance = axios.create(axiosConfig);

// 请求拦截
AxiosInstance.interceptors.request.use(
  (config) => {
    if (config.params?.manualBaseUrl) {
      config.baseURL = config.params.manualBaseUrl;
      delete config.params.manualBaseUrl;
    } else {
      config.baseURL = BASE_URL;
    }
    // 设置token
    config.headers.Authorization = 'token';

    // 请求拦截的第一个参数函数需要返回请求config
    return config;
  },
  (error) => Promise.reject(error)
);

// 响应拦截
AxiosInstance.interceptors.response.use(
  (response) => {
    // 如果响应整体里面有false 视为请求失败
    if (response.data && typeof response.data === 'object' && response.data.result === false) {
      return Promise.reject(response.data.error);
    }
    // 直接取出相应的data返回
    return response.data;
  },
  (error) => {
    const { response } = error;
    // 错误处理
    if (response && response instanceof Object) {
      return Promise.reject(response.data);
    }
    pendingAjax.clear();
    return Promise.reject(error);
  }
);

interface Fetch {
  get: <T = any>(url: any, qs?: any, responseType?: ResponseType) => Promise<AxiosResponse<T>>;
  delete: <T = any>(url: any, qs?: any, data?: any) => Promise<AxiosResponse<T>>;
  post: <T = any>(url: any, data: any, qs?: any) => Promise<AxiosResponse<T>>;
  patch: <T = any>(url: any, data: any, qs?: any) => Promise<AxiosResponse<T>>;
  put: <T = any>(url: any, data: any, qs?: any) => Promise<AxiosResponse<T>>;
}

// 进一步封装不同的请求方法
const fetch: Fetch = {
  get: (url, qs, responseType = 'json') => AxiosInstance.get(url, { params: qs, responseType }),
  delete: (url, qs = '', data) => AxiosInstance.delete(url, { params: qs, data }),
  post: (url, data, qs) => AxiosInstance.post(url, data, { params: qs }),
  patch: (url, data, qs) => AxiosInstance.patch(url, data, { params: qs }),
  put: (url, data, qs) => AxiosInstance.put(url, data, { params: qs }),
};

export default fetch;
