import fetch from '@/utils/request'

const { VITE_MOCK_URL } = import.meta.env

export const getProjects = (qs: any) => fetch.get('/projects_overview', { manualBaseUrl: VITE_MOCK_URL, ...qs })

export const delProject = (ids: string[]) => fetch.delete('/projects_overview', { manualBaseUrl: VITE_MOCK_URL }, {ids})

export const editProject = (id, data) => fetch.post(`/projects/${id}/detail`, data, { manualBaseUrl: VITE_MOCK_URL})