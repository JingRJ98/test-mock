import React, { FC, memo, useCallback, useEffect, useState } from 'react';
import { message, Input, Button } from 'antd'

import { getProjects, delProject, editProject } from '@/pages/Project/apis'
import ProjectItem from '@/pages/ProjectItem';

import styles from './style.module.scss';

const Project: FC = memo(() => {
  const [data, setData] = useState([])
  const [search, setSearch] = useState('')

  const getProjectsData = useCallback(async () => {
    try {
      const { data } = await getProjects({})
      setData(data)
    } catch (e) {
      message.error('has an error !');
    }
  }, [])

  useEffect(() => {
    getProjectsData?.()
  }, [getProjectsData])

  const onChange = useCallback((e) => {
    setSearch(e.target.value)
  }, [])

  const onSearch = useCallback(async () => {
    try {
      const { data } = await getProjects({ search })
      setData(data)
    } catch (e) {
      message.error('has an error !');
    }
  }, [search])

  const onDelete = useCallback(async (uid) => {
    try {
      await delProject([uid])
      getProjectsData()
    } catch (e) {
      message.error('has an error !');
    }
  }, [getProjectsData])

  const onEdit = useCallback(async(id, data) => {
    try {
      await editProject(id, data)
      getProjectsData()
    } catch (e) {
      message.error('has an error !');
    }
  }, [getProjectsData])

  return (
    <div className={styles.container}>
      <div className={styles.top}>
        <div className={styles.search}>
          <Input style={{ width: '80%' }} onChange={onChange} placeholder='输入name相关的搜索' />
          <Button onClick={onSearch}>搜索name</Button>
        </div>
        <div style={{ fontSize: '20px', fontWeight: 500 }}>
          当前检索到数据 共: {data.length} 条
        </div>
      </div>
      <div className={styles.content}>
        {data.map((obj) => <ProjectItem onDelete={onDelete} onEdit={onEdit} key={obj.id} {...obj} />)}
      </div>
    </div>
  )
});

export default Project;