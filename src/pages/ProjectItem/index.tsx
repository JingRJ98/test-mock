import React, { FC, memo, useCallback, useRef } from 'react';
import { Button, Modal, Input } from 'antd'

import styles from './style.module.scss';

interface IProps {
  id: string;
  name: string;
  des: string;
  my_type: string;
  my_type2: string;
  participators: string;
  create_at: string;
  onDelete: (ids: string[]) => void;
  onEdit: (id: string, data: any) => void
}

const ProjectItem: FC<IProps> = memo((props) => {
  const {
    name,
    id,
    des,
    my_type,
    my_type2,
    participators,
    create_at,
    onDelete,
    onEdit,
  } = props;

  const newName = useRef('')

  const onOk = useCallback(() => {
    onEdit(id, {name: newName.current})
  }, [id, onEdit])

  const handleEdit = useCallback(() => {
    Modal.confirm({
      title: '修改name',
      content: <Input placeholder='请输入新name' onChange={(e) => {newName.current = (e.target.value)}}/>,
      onOk,
      width: 440,
    })
  }, [onOk])

  return (
    <div className={styles.container}>
      <div className={styles.btn}>
        <Button onClick={handleEdit}> 改name </Button>
        <Button onClick={() => onDelete(id)}> 删除 </Button>
      </div>
      <div>
        <p style={{fontSize: '20px', fontWeight: 500, color: '#def'}}>name: {name}</p>
        <p>participators: {participators}</p>
        <p>des: {des}</p>
        <p>create_at: {create_at}</p>
        <p>my_type: {my_type}</p>
        <p>my_type: {my_type2}</p>
      </div>
    </div>
  )
});
export default ProjectItem;