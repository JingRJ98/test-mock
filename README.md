# 无侵入代码的前端mock方案
## 背景&痛点
前后端在进行技术设计评审之后针对接口达成一致,但是各自开发过程中往往会出现以下情况: 
1. 前端开发页面完成了,后端接口还没好,或者前端和后端同一时间没有开发同一模块, 最后联调时间被压缩的特别紧张
2. 前端在项目代码中留下一些mock的数据,提交代码时忘记删干净,删完下次修改时又要写
3. 前端mock数据不够多(比如列表数据需要100+条, 前端得**手动**mock一个长度100+的数组,然后每个元素还得定义id, data等数据)

## 技术栈
React + Node + json-server + Mockjs + vite
**主要思路:** 借助json-server在本地建立mock后端服务, 开启两套环境, 自定义接口路由和灵活的中间件等, 借助Mockjs生成数据, 基于约定的接口文档传递数据,因此对前端项目代码无任何改动, 打包的时候不会将mock相关的代码打包进去

# 与市面上其他mock方案的比较
1. 前端响应拦截(单一mockjs)
这种方式虽然实现了Mock与代码的部分解耦，但无法完全和业务代码解耦（因为必须挂载一个JS并进行Mock配置）。
虽然提供了大量的Mock API，但是也仍然无法发出真实的网络请求，模拟真实度不够。
另外这种方式因为是对XHR对象的改写，有些情况下兼容性并不好，比如IE8等低版本浏览器，还有较新的Fetch API也拦截不到

2. 借助一些代理软件(Fiddler、Charles)拦截
缺点是操作复杂, 可移植性不强

3. 借助后端的接口管理工具来返回前端mock数据
缺点: 后端不愿意


# 具体设计
## 主文件结构
新增**mocks**文件夹
```
.
├── README.md
├── index.html
├── mocks         <====
├── package.json
├── public
├── src
├── tsconfig.json
├── tsconfig.node.json
└── vite.config.ts
```

## mocks文件结构
db: 包含模拟生成的数据库
public: 涉及静态资源文件存放
router: 真实后端接口路由和mock路由的映射
utils: 包含一些读取文件, 自定义路由, 自定义随机数据的生成方式的工具函数
```
├── db
│   └── project.json
├── index.js
├── public
├── router
│   ├── index.js
│   ├── project.js
│   └── routes
│       └── project.json
└── utils
    ├── babelConfig.js
    ├── middleware.js
    ├── random.js
    └── routes.js
```

## 实现
1. package.json新增两个命令
  - `"mock": "vite --mode mock"`
    用于切换NODE, 针对这个NODE使用不同的环境变量, 如果是mock变量就修改请求的BASE_URL
  - `"mock:server": "NODE_ENV=mock nodemon './mocks/index' --watch './mocks/**/*'"`
    用于启动一个本地的服务,用来模拟后端, nodemon执行热更新

新增.env.development和.env.mock文件用于储存环境变量
2. **数据库**使用glob读取/db下的json文件, mockjs来解析db中的数据
3. **路由**使用glob读取/router下的json文件(真实后端接口和mock接口的映射文件), jsonServer.rewriter来重写接口
4. 使用路由中间件来对req, res进行一些额外处理, 例如:
```js
const setCustomRoute = (server) => {
  server.post('/project/:id/detail', (req, res, next) => {
    // 改变url
    req.url = `/projects_overview/${req.params.id}`
    // 改变方法
    req.method = 'PATCH'
    //////////////////////////////////////////////////////////////////////
    // 重写数据库
    const data = db.getState()
    data.projects_overview.forEach((item) => {
      if(item.id === id){
        item.name = name
      }
    })
    db.write()
    res.json({result: true})
    next()
  })
}
```
5. mockjs库里面提供Random可以封装一些自己需要的生成随机数据的方式

## 注意事项
- 演示项目默认使用vite + ts搭建, vite引入js包需要写全文件后缀
- mockjs是一个commonjs包, vite中引入需要改变语法
```js
// commonjs包不能直接用 import {} from 'mockjs'语法
import pkg from 'mockjs';
const { Random } = pkg;
```

## 优点
针对上面问题开发的mock工作流方案:
1. 改动小, 只有接口的baseUrl有改动, 项目代码无任何改动
2. 可复用, mock的代码会一直保留, 但不会被打包进构建产物
3. 可定制, 各种自定义中间件处理数据,非常灵活; 数据实现热加载，方便调试
4. 对前端技术水平的成长, 完全模拟了一个简单的后端工作