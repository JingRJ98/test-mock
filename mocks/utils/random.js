// commonjs包不能直接用
// import {} from 'mockjs'语法
import pkg from 'mockjs';
const { Random } = pkg;

export const extendRandom = () => {
  Random.extend({
    my_type: function () {
      const arr = ['Type1', 'Type2', 'Type3']
      return this.pick(arr)
    },
    my_type2: function () {
      const arr = ['A', 'B', 'C', 'D', 'E']
      // 随机选择 1个 或者2个 或者3个输出
      return this.pick(arr, 1, 3)
    }
  })
}