export default function () {
  return {
    presets: [
      [
        '@babel/preset-env',
        {
          targets: { node: 'current' },
          modules: 'commonjs',
        }
      ]
    ],
    plugins: [
      'module-resolver',
      {
        root: ['./mocks'],
        alias: {
          test: './test'
        }
      }
    ],
    extensions: ['.js', '.ts'],
    babelrc: false,
    cache: false
  }
}