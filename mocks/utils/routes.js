import glob from 'glob'
import mockjs from 'mockjs'
import fs from 'fs'

/**
 * 扫描route文件夹 生成自定义的路由数据
 * @param {string} filePath 
 */
export const getCustomRoutes = (filePath) => {
  const data = {}
  const routerFiles = glob.sync(filePath)
  routerFiles.forEach((p) => {
    const json = fs.readFileSync(p, 'utf-8')
    Object.assign(data, JSON.parse(json))
  })

  return data
}

/**
 * mockjs解析json文件
 * @param {string} filePath
 */
const parsingToMockjs = (filePath) => {
  const data = fs.readFileSync(filePath, 'utf-8')
  return mockjs.mock(JSON.parse(data))
}

/**
 * 扫描db目录, 生成mock数据
 */
export const getMockData = (filePath) => {
  const data = {}
  const mockFiles = glob.sync(filePath)
  mockFiles.forEach((p) => {
    const json = parsingToMockjs(p)
    Object.assign(data, json)
  })
  return data
}