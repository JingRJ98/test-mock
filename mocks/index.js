import path from 'path'
import { fileURLToPath } from 'url'
import jsonServer from 'json-server'

import customMiddleware from './utils/middleware.js'
import { getMockData, getCustomRoutes } from './utils/routes.js'
import setCustomRoutes from './router/index.js'
import {extendRandom} from './utils/random.js'
// 借助mockjs的random函数封装一些自己需要的随机数据
extendRandom()

const __dirname = path.dirname(fileURLToPath(import.meta.url))

const server = jsonServer.create()

const middlewares = jsonServer.defaults({
  bodyParser: true,
  static: path.join(__dirname, './public')
})

const router = jsonServer.router(getMockData(path.join(__dirname, '/db/*.json')))

server.use(jsonServer.bodyParser)
server.use(middlewares)
// 自定义的路由操作
setCustomRoutes(server, router)
// 自定义的通用中间件
server.use(customMiddleware)
// 将真实后端的url路由重写为自定义的mock路由
server.use(jsonServer.rewriter(getCustomRoutes(path.join(__dirname, '/router/**/*.json'))))
server.use(router)

// 响应数据的封装
router.render = (req, res) => {
  res.jsonp({
    success: true,
    code: 200,
    msg: 'success',
    data: res.locals.data
  })
}

server.listen(3100, () => {
  console.log('Mock server is running ~');
})
