export const setCustomRoute = (server, router) => {
  // 删除
  server.delete('/projects_overview', async (req, res, next) => {
    await res.setHeader('Access-Control-Allow-Origin', '*');
    req.url = `/projects_overview/${req?.body?.ids[0]}`
    next()
  })
  // 编辑
  server.post('/projects/:id/detail', (req, res, next) => {
    const { id } = req.params
    const { name } = req.body
    const { db } = router
    const data = db.getState()
    data.projects_overview.forEach((item) => {
      if(item.id === id){
        item.name = name
      }
    })
    db.write()
    res.json({result: true})
    next()
  })
}

export default setCustomRoute