import setProjectRoute from './project.js'

const setCustomRoutes = (server,router) => {
  setProjectRoute(server, router)
}

export default setCustomRoutes